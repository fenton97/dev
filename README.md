# ROUTING CONTROLLER TEST
Small test with nodejs, typescript and routing-controllers. Project files are 
located under the src folder.

## COMANDS ##

### INSTALL
```
$ npm istall
```

### TEST
```
$ ts-node src/app.ts
```

### BUILD 
```
$ tsc
```